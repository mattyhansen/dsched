package main

import (
	"errors"
	ds "gitlab.com/andreynech/dsched/proto"
	"gopkg.in/robfig/cron.v2"
	"log"
	"os/exec"
	"strings"
	"sync"
)

// Global task list container
var (
	globalTaskList = &ds.TaskList{}
	jobrunner      = cron.New()
)

type command struct {
	cmdLine string
}

func (c command) Run() {
	log.Printf("Running: %s", c.cmdLine)
	args := strings.Split(c.cmdLine, " ")
	var cmd *exec.Cmd
	if len(args) > 1 {
		cmd = exec.Command(args[0], args[1:]...)
	} else {
		cmd = exec.Command(c.cmdLine)
	}
	// err := cmd.Start()
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("Error: %v", err)
		return
	}
	log.Println(string(out))
}

// Mutex to synchronize global task list access
var taskMutex = sync.Mutex{}

func listImpl() (*ds.TaskList, error) {
	taskMutex.Lock()
	defer taskMutex.Unlock()
	return globalTaskList, nil
}

func addImpl(req *ds.Task) (*ds.Task, error) {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	cmd := command{cmdLine: req.GetAction()}
	entryID, err := jobrunner.AddJob(req.GetCron(), cmd)
	if err != nil {
		return req, err
	}

	req.Id = uint32(entryID)
	globalTaskList.Tasks = append(globalTaskList.GetTasks(), req)
	error := saveTaskList(globalTaskList)
	return req, error
}

func removeImpl(req *ds.Task) error {
	del := false
	var err error

	taskMutex.Lock()
	defer taskMutex.Unlock()

	for i, v := range globalTaskList.GetTasks() {
		if v.GetId() == req.GetId() {
			// Remove i-th element from slice
			globalTaskList.Tasks = append(globalTaskList.GetTasks()[:i],
				globalTaskList.GetTasks()[i+1:]...)
			err = saveTaskList(globalTaskList)
			del = true
			break
		}
	}

	if !del {
		return errors.New("Task ID not found")
	}

	jobrunner.Remove(cron.EntryID(req.GetId()))

	return err
}

func removeAllImpl() error {
	taskMutex.Lock()
	defer taskMutex.Unlock()
	for _, v := range globalTaskList.GetTasks() {
		jobrunner.Remove(cron.EntryID(v.GetId()))
	}
	globalTaskList.Tasks = make([]*ds.Task, 0)
	error := saveTaskList(globalTaskList)
	return error
}
